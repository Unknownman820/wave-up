<resources>
    <string name="prefs_service">Zerbitzua</string>
    <string name="pref_enable">Gaitu</string>

    <string name="prefs_modes">WaveUp moduak</string>
    <string name="pref_wave_mode">Keinu modua</string>
    <string name="pref_wave_mode_summary">Pantaila piztu hurbiltasun sentsorearen gainean keinu egitean.</string>
    <string name="pref_pocket_mode">Poltsiko modua</string>
    <string name="pref_pocket_mode_summary">Pantaila piztu mugikorra poltsiko edo poltsatik ateratzerakoan.</string>

    <string name="pref_lock_screen">Pantaila blokeatu</string>
    <string name="pref_lock_screen_mode_summary">Hurbiltasun sentsorea estaltzen denean pantaila itzali eta blokeatu.</string>
    <string name="pref_lock_screen_when_landscape">Blokeatu paisaia moduan</string>
    <string name="pref_lock_screen_when_landscape_summary">Ezgaitu ezazu hau gailua horizontalki duzunean pantaila blokeatzea saihesteko, adibidez bideo edo argazki bat ikusterakoan.</string>
    <string name="pref_lock_screen_with_power_button_as_root">Fingerprint eta Smart Lockerako konponketa</string>
    <string name="pref_lock_screen_with_power_button_as_root_summary">Pantaila itzali pizteko botoia sakatu dela simulatuz. Root behar du.</string>
    <string name="pref_lock_screen_vibrate_on_lock">Dardaratu blokeatu aurretik</string>
    <string name="pref_lock_screen_vibrate_on_lock_summary">Hurbiltasuna detektatu delaz jakinarazten zaitu gailua nahi gabe blokeatzea saihesteko.</string>
    <string name="pref_lock_screen_app_exception">Baztertutako aplikazioak</string>
    <string name="pref_lock_screen_app_exception_summary">Blokeatzea ekidin aplikazio hauetakoren bat aurreko planoan badago. Sakatu zerrenda ireki eta editatzeko.</string>
    <string name="pref_sensor_cover_time_before_locking_screen">Estaltze denbora blokeatu aurretik</string>
    <string name="pref_sensor_cover_time_before_locking_screen_summary">Hurbiltasun sentsorea estalita egon behar den denbora pantaila blokeatu aurretik. Oraingo balioa: %s.</string>
    <string name="pref_notification_section">Jakinarazpena</string>
    <string name="pref_show_notification">Jakinarazpena erakutsi</string>
    <!-- From Pie onwards, we just show the Android system settings and always use a ForegroundService -->
    <string name="pref_show_notification_v28">Jakinarazpenen ezarpenak</string>
    <string name="wave_up_service_started">WaveUp martxan da</string>
    <string name="wave_up_service_stopped">WaveUp ez dago martxan</string>
    <string name="tap_to_open">Sakatu WaveUp irekitzeko</string>
    <string name="pause">Pausatu</string>
    <string name="resume">Berrekin</string>
    <string name="disable">Gelditu</string>

    <string name="something_went_wrong">Zerbait txarto joan da</string>
    <string name="root_access_failed">Ezin izan da root sarrera lortu</string>

    <string name="uninstall_button">WaveUp desinstalatu</string>
    <string name="removed_device_admin_rights">Administratzaile baimenak kendu dira. \'Pantaila blokeatu\' aukera aktibatu ezazu hauek berriro eskuratzeko</string>

    <!-- Phone permission dialog -->
    <string name="phone_permission_yes">Eskatu</string>
    <string name="phone_permission_no">Baztertu</string>
    <!-- I put this array here instead of in arrays.xml because it has to be translated. -->
    <string-array name="sensor_cover_time_entries">
        <item>Berehala</item>
        <item>0,5 segundu</item>
        <item>Segundu 1</item>
        <item>1,5 segundu</item>
        <item>2 segundu</item>
        <item>5 segundu</item>
    </string-array>

    <!-- Vibrate on lock -->
    <string name="pref_vibrate_on_lock_time_title">Dardaratu blokeatu aurretik</string>
    <string name="pref_number_of_waves">Keinuen kopurua</string>
    <string name="pref_number_of_waves_summary">Hurbiltasun sentsorearen gainean keinu egin behar dituzun aldiak (estali eta desestali) zure gailua esnatzeko.\n\nOraingo balioa: %s.</string>

    <string-array name="number_of_waves_entries">
        <item>1 (estali, desestali)</item>
        <item>2 (estali, desestali, estali, desestali)</item>
        <item>3 (estali, desestali, estali, desestali, estali, desestali)</item>
    </string-array>

    <string name="privacy_policy_menu_item">Pribatutasun politika</string>
    <string name="licenses_menu_item">Lizentziak</string>
    <!-- Excluded apps from locking -->
    <string name="untitled_app">Aplikazio izengabea</string>
    <string name="exclude_apps_activity_title">Aplikazioak baztertu</string>
    <string name="excluded_apps">Baztertutako aplikazioak</string>
    <string name="not_excluded_apps">Aplikazio ez baztertuak</string>
    <string name="exclude_apps_text">
Ez dago baztertutako aplikaziorik une honetan.
\n\nHemen zerrendatutako aplikazioak baztertuak izango dira eta WaveUpek
ez du pantaila blokeatuko hauetako bat irekita badago.
\n\nBesterik gabe sakatu beheko aplikazio batean zerrendara gehitzeko.
Sakatu berriz ezabatzeko iritziz aldatzen baduzu.
</string>
    <!-- Lack of proximity sensor -->
    <string name="missing_proximity_sensor_title">Hurbiltasun sentsorea falta da</string>
    </resources>
