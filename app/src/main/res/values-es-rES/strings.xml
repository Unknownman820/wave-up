<resources>
    <string name="prefs_service">Servicio</string>
    <string name="pref_enable">Activar</string>

    <string name="prefs_modes">Modos</string>
    <string name="pref_wave_mode">Modo \"wave\"</string>
    <string name="pref_wave_mode_summary">La pantalla se encenderá cuando pases la mano por encima del sensor de proximidad.</string>
    <string name="pref_pocket_mode">Modo bolsillo</string>
    <string name="pref_pocket_mode_summary">La pantalla se encenderá cuando saques el móvil del bolsillo o del bolso.</string>

    <string name="pref_lock_screen">Bloquear la pantalla</string>
    <string name="pref_lock_screen_mode_summary">La pantalla se apagará y bloqueará cuando tapes el sensor de proximidad.</string>
    <string name="pref_lock_screen_when_landscape">Bloquear en modo apaisado</string>
    <string name="pref_lock_screen_when_landscape_summary">Desactivar esta opción para prevenir el bloqueo del móvil mientras esté en modo apaisado, por ejemplo a la hora de ver vídeos.</string>
    <string name="pref_lock_screen_with_power_button_as_root">Apaño para huella dactilar y Smart Lock</string>
    <string name="pref_lock_screen_with_power_button_as_root_summary">Apaga la pantalla simulando el botón de encendido/apagado. Necesita root.</string>
    <string name="pref_lock_screen_vibrate_on_lock">Vibrar antes de bloquear</string>
    <string name="pref_lock_screen_vibrate_on_lock_summary">Avisa mediante una vibración al detectar un cambio en la proximidad para evitar bloquear el móvil sin querer.</string>
    <string name="pref_lock_screen_app_exception">Apps excluidas</string>
    <string name="pref_lock_screen_app_exception_summary">La pantalla no se bloqueará si una de las siguientes apps está abierta. Toca aquí para abrir la lista y editarla.</string>
    <string name="pref_sensor_cover_time_before_locking_screen">Tiempo de tapado antes de bloquear</string>
    <string name="pref_sensor_cover_time_before_locking_screen_summary">Tiempo que hay que cubrir el sensor de proximidad para que se bloquee el móvil. Valor actual: %s.</string>
    <string name="pref_notification_section">Notificación</string>
    <string name="pref_show_notification">Mostrar notificación</string>
    <string name="pref_show_notification_summary">Muestra una notificación permanente que permite controlar WaveUp y que asegura que se ejecute sin interrupción.</string>
    <!-- From Pie onwards, we just show the Android system settings and always use a ForegroundService -->
    <string name="pref_show_notification_v28">Opciones de notificación</string>
    <string name="pref_show_notification_summary_v28">Toca aquí para abrir las opciones de notificación de Android para WaveUp.\n\n<small>Nota del desarrollador: a partir de Android 9, no se les permite a las apps leer sensores cuando están en segundo plano a menos que muestren una notificación permanente. Sin embargo, el usuario puede ocultarlas manualmente utilizando la configuración del sistema. Dependiendo de tu móvil, es posible que no funcione del todo bien. Si éste es el caso, prueba a activar la notificación de nuevo.</small></string>

    <string name="wave_up_service_started">WaveUp está ejecutándose</string>
    <string name="wave_up_service_stopped">WaveUp no se está ejecutando</string>
    <string name="tap_to_open">Toca para abrir WaveUp</string>
    <string name="pause">Pausar</string>
    <string name="resume">Reanudar</string>
    <string name="disable">Parar</string>

    <string name="lock_admin_rights_explanation"><b> Bloquear dispositivo: Administrador del dispositivo. </b> \n\n Para poder bloquear la pantalla, necesitamos establecer WaveUp como un Administrador del dispositivo. \n\n Una vez que una app es un Administrador del Dispositivo, no podrás desinstalarla normalmente sin haber revocado el Permiso de Administración de Dispositivo. \n\n Si deseas revocar el Permiso de Administrador del Dispositivo, simplemente ve a <i> Configuración > Seguridad > Administradores del Dispositivo </i> y desmarca WaveUp. \n\n Si deseas desinstalar WaveUp solo toca en el botón de \"Desinstalar WaveUp\" hasta abajo en la app, lo cual revoca el Privilegio de Administrador de Dispositivo <i> y </i> directamente desinstala WaveUp. \n\n ¿Estás seguro que quieres establecer WaveUp como Administrador del Dispositivo?</string>
    <string name="something_went_wrong">Algo fue mal</string>
    <string name="root_access_failed">Acceso root denegado</string>

    <string name="uninstall_button">Desinstalar WaveUp</string>
    <string name="removed_device_admin_rights">WaveUp ha perdido los derechos administrativos. Activa la opción \"Bloquear la pantalla\" para volver a activarlos.</string>

    <!-- Phone permission dialog -->
    <string name="phone_permission_yes">Pedir</string>
    <string name="phone_permission_no">Ignorar</string>
    <!-- I put this array here instead of in arrays.xml because it has to be translated. -->
    <string-array name="sensor_cover_time_entries">
        <item>Inmediatamente</item>
        <item>0,5 segundos</item>
        <item>1 segundo</item>
        <item>1,5 segundos</item>
        <item>2 segundos</item>
        <item>5 segundos</item>
    </string-array>

    <!-- Vibrate on lock -->
    <string name="pref_vibrate_on_lock_time_title">Vibrar antes de bloquear</string>
    <string name="pref_number_of_waves">Número de pasadas</string>
    <string name="pref_number_of_waves_summary">Número de veces que debes pasar la mano por encima del sensor de proximidad (cubrir y descubrir) para que se encienda la pantalla de tu móvil. Valor actual: %s.</string>

    <string-array name="number_of_waves_entries">
        <item>1 (cubrir, descubrir)</item>
        <item>2 (cubrir, descubrir, cubrir, descubrir)</item>
        <item>3 (cubrir, descubrir, cubrir, descubrir, cubrir, descubrir)</item>
    </string-array>

    <string name="privacy_policy_menu_item">Política de privacidad</string>
    <string name="licenses_menu_item">Licencias</string>
    <!-- Excluded apps from locking -->
    <string name="untitled_app">App sin nombre</string>
    <string name="exclude_apps_activity_title">Exlcuir apps</string>
    <string name="excluded_apps">Apps excluidas</string>
    <string name="not_excluded_apps">Apps no excluidas</string>
    <string name="exclude_apps_text">        No hay apps excluidas.
        \n\nSi una de las apps que aparecen en esta lista está abierta,
        WaveUp no bloquerá la pantalla.
        \n\nToca una de las apps de abajo para añadirlas a la lista. Si cambias
        de opinión, vuelve a tocarla para eliminarla de la lista.
    </string>
    <!-- Lack of proximity sensor -->
    <string name="missing_proximity_sensor_title">Sensor de proximidad no disponible</string>
    <string name="missing_proximity_sensor_text">WaveUp no puede funcionar sin un sensor de proximidad. O tu móvil carece de sensor de proximidad o WaveUp no puede acceder a él. ¡Lo sentimos!</string>
</resources>
