WaveUp to aplikacja, która <i>wybudza twój telefon</i> - włącza ekran - kiedy <i>machasz</i> nad sensorem zbliżenia.

Napisałem tę aplikację, bo chciałem uniknąć wciskania przycisku włączenia tylko po to, żeby sprawdzić godzinę - co akurat robię dosyć często na moim telefonie. Są też inne aplikacje, które robią dokładnie to i nawet więcej. Byłem zainspirowany przez "Gravity Screen On/Off", które jest <b>świetną</b> aplikacją. Jednak, jestem wielkim fanem open source i próbuję instalować wolne oprogramowanie (wolne, jako wolność) na moim telefonie jeśli jest to możliwe. Nie byłem w stanie znaleźć żadnej aplikacja open source, która by to robiła, więc po prostu zrobiłem ją sam. Jeśli jesteś zainteresowany, możesz popatrzeć na kod źródłowy:
https://gitlab.com/juanitobananas/wave-up

Po prostu pomachaj ręką nad sensorem zbliżenia, aby włączyć ekran. Nazywa się to <i>tryb machania</i> i może być wyłączony w zakładce opcji, żeby uniknąć przypadkowego włączania ekranu. 

Włączy to też ekran, kiedy wyciągasz smartfon z kieszeni, albo torebki. To <i>nazywa się tryb kieszeniowy</i> i też może być wyłączony w zakładce opcji.

Oba te tryby są domyślnie włączone.

Zablokowuje też twój telefon i wyłącza ekran jeśli zakryjesz sensor zbliżenia przez jedną sekundę (albo wybrany czas). Nie ma to jeszcze żadnej specjalnej nazwy, ale również może być zmienione w zakładce opcji. Nie jest to włączone domyślnie.

Dla tych co nigdy nie słyszeli o sensorze zbliżenia: jest to małe urządzonko, które jest gdzieś niedaleko miejsca do którego przybliżasz ucho, żeby rozmawiać przez telefon. Praktycznie nie da się go zobaczyć i jest odpowiedzialny za informowanie twojego telefonu o tym, żeby wyłączył ekran, kiedy rozmawiasz. 

<b>Odinstaluj</b>

Ta aplikacja używa uprawnienia Administratora Urządzenia. Dlatego też nie możesz odinstalować WaveUp 'normalnie'.

Aby odinstalować, po prostu otwórz aplikację i użyj przycisku 'Odinstaluj WaveUp' na dole menu.

<b>Znane problemy</b>

Niestety niektóre smartfony zostawiają procesor włączony, nasłuchując informacji od sensora zbliżenia. Nazywa się to <i>czujne zablokowanie</i> (wake lock) i powoduje znaczne zużycie baterii. Nie jest to moja wina i nic z tym nie mogę zrobić, żeby to zmienić. Inne telefony przejdą w tryb uśpienia, jednocześnie nasłuchując informacji od sensora zbliżenia. W tym przypadku zużycie baterii jest praktycznie zerowe.

<b>Wymagane Uprawnienia Androida:</b>

▸ WAKE_LOCK żeby włączyć ekran
▸ USES_POLICY_FORCE_LOCK żeby zablokować urządzenie
▸ RECEIVE_BOOT_COMPLETED żeby automatycznie włączyć się przy starcie urządzenia, jeśli zostanie to włączone
▸ READ_PHONE_STATE żeby wstrzymać WaveUp przy rozmowie telefonicznej

<b>Różne informacje</b>

To jest pierwsza aplikacja Androidową, jaką napisałem, więc uwaga!

Jest to też mój mały wkład w świat open source. Wreszcie!

Miło by mi było, gdybyś mógł dać mi feedback jakiegokolwiek rodzaju lub włożyć swój wkład w jakikolwiek sposób!

Dzięki za przeczytanie!

Open source rządzi!!!

<b>Tłumaczenia</b>

Byłoby bardzo fajnie jakbyś mógł pomóc przetłumaczyć WaveUp na swój język (nawet angielska wersja mogłaby być przejrzana).
Można przetłumaczyć przez dwa projekty na Transifex: https://www.transifex.com/juanitobananas/waveup/ i https://www.transifex.com/juanitobananas/libcommon/.

<b>Podziękowania</b>

Moje specjalne podziękowania dla:

Zobacz: https://gitlab.com/juanitobananas/wave-up/#acknowledgments